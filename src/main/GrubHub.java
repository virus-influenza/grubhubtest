/**
 * Francisco Viejobueno Muñoz
 * Test GrubHub 1
 */
public class GrubHub {
    private static final char WORD_SPLIT_CHAR     = ' ';
    private static final String WORD_SPLIT_STRING = " ";

    /**
     * example with no split
     *
     * @param sample phrase to reverse words
     * @return reversed words
     */
    public static String reverseWords(String sample) {
        String word = "";
        String result = "";
        // iterate sample string to collect words
        for (int i = 0; i < sample.length(); i++) {
            char current = sample.charAt(i);
            // if we are in a word, copy and continue
            if (current != WORD_SPLIT_CHAR) {
                word += current;
                continue;
            }
            // if we are not in a word, copy character, add to result and clear word
            word += current;
            result = word += result;
            word = "";
        }

        // add last word
        word += WORD_SPLIT_CHAR;
        result = word += result;
        // return result
        return result.trim();
    }

    /**
     * example with split
     *
     * @param sample phrase to reverse words
     * @return reversed words
     */
    public static String reverseWordsSplit(String sample) {
        // split sample phrase in words
        String[] split = sample.split(WORD_SPLIT_STRING);
        // reverse array
        for (int i = 0; i < split.length / 2; i++) {
            String word = split[i];
            split[i] = split[split.length - i - 1];
            split[split.length - i - 1] = word;
        }
        // join array by word separator and return
        return String.join(WORD_SPLIT_STRING, split);
    }
}