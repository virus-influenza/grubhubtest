
import org.junit.Assert;
import org.junit.Test;

/**
 */
public class GrubHubTest {

    @Test
    public void test_reverse_split_sample_one(){
        String result = GrubHub.reverseWordsSplit("The quick brown fox jumps over the lazy dog.");

        Assert.assertEquals("dog. lazy the over jumps fox brown quick The", result);
    }

    @Test
    public void test_reverse_split_sample_two(){
        String result = GrubHub.reverseWordsSplit("The quick brown fox jumps over the lazy dog.");

        Assert.assertEquals("dog. lazy the over jumps fox brown quick The", result);
    }

    @Test
    public void test_reverse_split_sample_three(){
        String result = GrubHub.reverseWordsSplit("111 W Washington St, Chicago, IL 60602");

        Assert.assertEquals("60602 IL Chicago, St, Washington W 111", result);
    }

    @Test
    public void test_reverse_sample_one(){
        String result = GrubHub.reverseWords("The quick brown fox jumps over the lazy dog.");

        Assert.assertEquals("dog. lazy the over jumps fox brown quick The", result);
    }

    @Test
    public void test_reverse_sample_two(){
        String result = GrubHub.reverseWords("The quick brown fox jumps over the lazy dog.");

        Assert.assertEquals("dog. lazy the over jumps fox brown quick The", result);
    }

    @Test
    public void test_reverse_sample_three(){
        String result = GrubHub.reverseWords("111 W Washington St, Chicago, IL 60602");

        Assert.assertEquals("60602 IL Chicago, St, Washington W 111", result);
    }
}
